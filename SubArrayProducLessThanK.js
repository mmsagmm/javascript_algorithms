/**
 * @param {number[]} nums
 * @param {number} k
 * @return {number}
 */
var numSubarrayProductLessThanK = function (nums, k) {

    if (nums.length < 1) {
        return 0;
    }
    let cnt = 0;
    let products = [];

    products[0] = nums[0];

    if (products[0] < k) cnt++;

    let subArrayStartIndex = 0;

    for (let i = 1; i < nums.length; i++) {
        let value = products[i - 1] * nums[i];
      

        if (value < k) {
            products[i] = value;
            cnt += (i - subArrayStartIndex + 1);
        }
        else {
            while (value >= k) {

                value = value / nums[subArrayStartIndex];
                subArrayStartIndex++;
                if (subArrayStartIndex >= i) {
                    i = subArrayStartIndex;
                    value = nums[i];
                    if (subArrayStartIndex >= nums.length) break;
                }
            }

            if (i >= nums.length) break;
            products[i] = value;
            if (value < k) {
                cnt += (i - subArrayStartIndex + 1);
            }
        }
    }

    return cnt;
}

var printSubArr = function (nums, s, e) {
    let str = 'ee:';
    for (let i = s; i <= e; i++) {
        str += nums[i];
    }
    console.log(str);
}



// console.log(numSubarrayProductLessThanK([10, 5, 2, 6], 100));

let input = [10, 5, 2, 6];

console.log(numSubarrayProductLessThanK(input, 100));