/**
 * @param {number} numRows
 * @return {number[][]}
 */
var generate = function (numRows) {
    let target = [];
    if (numRows == 0) return target;

    target[0] = [1];
    if (numRows == 1) return target;

    target[1] = [1, 1];
    if (numRows == 2) return target;

    for (let i = 2; i < numRows; i++) {
        let line = [1];
        for (let j = 0; j < target[i - 1].length - 1; j++) {
            line.push(target[i - 1][j] + target[i - 1][j + 1]);
        }
        line.push(1)
        target.push(line);
    }

    return target;
};


console.log(generate(5))